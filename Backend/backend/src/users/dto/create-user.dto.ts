import { IsEmail, IsNotEmpty, Length } from 'class-validator'
export class CreateUserDto {
  // id: number;

  @IsEmail()
  email: string;

  @IsNotEmpty()
  @Length(3, 32)
  password: string;

  @IsNotEmpty()
  @Length(3, 32)
  fullName: string;

  // @IsArray()
  // @ArrayNotEmpty()
  // roles: ('admin' | 'user')[];

  @IsNotEmpty()
  gender: 'male' | 'female' | 'others';
}
